package sfedu.optimisationmethods;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.ArrayList;
import java.util.List;

public class Vertex extends Circle {

    final protected List<Edge> edges;
    final protected List<Edge> remainingFlowEdges;
    protected Edge bfsFrom;
    protected boolean bfsUsed;

    public Vertex() {
        super(0, 0, 12.0);
        bfsFrom = null;
        bfsUsed = false;
        edges = new ArrayList<>();
        remainingFlowEdges = new ArrayList<>();
        setFill(Color.WHITE);
        setStroke(Color.BLACK);
    }
}
