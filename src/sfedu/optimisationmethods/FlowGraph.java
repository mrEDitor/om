package sfedu.optimisationmethods;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

import java.util.*;

public class FlowGraph {

    private final List<Vertex> vertices;
    private Pane view;
    private Label sourceLabel, targetLabel;
    private Vertex source, target;
    private DoubleProperty maxFlow;

    public FlowGraph() {
        vertices = new ArrayList<>();
        sourceLabel = new Label("s");
        hideLabel(sourceLabel);
        sourceLabel.setMouseTransparent(true);
        targetLabel = new Label("t");
        hideLabel(targetLabel);
        targetLabel.setMouseTransparent(true);
        maxFlow = new SimpleDoubleProperty(0);
    }

    private void hideLabel(Label label) {
        label.layoutXProperty().unbind();
        label.layoutYProperty().unbind();
        label.setLayoutX(-50);
        label.setLayoutY(-50);
    }

    public void setSource(Vertex vertex) {
        source = vertex;
        sourceLabel.layoutXProperty().bind(source.layoutXProperty().subtract(8));
        sourceLabel.layoutYProperty().bind(source.layoutYProperty().subtract(12));
        sourceLabel.toFront();
        refreshMaxFlow();
    }

    public void setTarget(Vertex vertex) {
        target = vertex;
        targetLabel.layoutXProperty().bind(target.layoutXProperty().add(2));
        targetLabel.layoutYProperty().bind(target.layoutYProperty().subtract(6));
        targetLabel.toFront();
        refreshMaxFlow();
    }

    public void addVertex(Vertex vertex) {
        vertices.add(vertex);
        view.getChildren().add(vertex);
    }

    public void deleteVertex(Vertex vertex) {
        while (!vertex.edges.isEmpty()) {
            Edge edge = vertex.edges.get(0);
            edge.getSource().edges.remove(edge);
            edge.getTarget().edges.remove(edge.getInverted());
            view.getChildren().removeAll(edge, edge.getInverted(), edge.getLabel());
        }
        if (vertex == source) {
            hideLabel(sourceLabel);
        }
        if (vertex == target) {
            hideLabel(targetLabel);
        }
        vertices.remove(vertex);
        view.getChildren().remove(vertex);
        refreshMaxFlow();
    }

    public void setView(Pane view) {
        this.view = view;
        view.getChildren().addAll(sourceLabel, targetLabel);
    }

    public DoubleProperty maxFlowProperty() {
        return maxFlow;
    }

    public void refreshMaxFlow() {
        if (source == target) {
            maxFlow.setValue(Double.POSITIVE_INFINITY);
        } else if (source == null || target == null) {
            maxFlow.setValue(0);
        } else {
            double ans = 0;
            // Refresh remaining capacities
            for (Vertex vertex : vertices) {
                for (Edge edge : vertex.edges) {
                    Edge e = new Edge(edge.getSource(), edge.getTarget(), null, Integer.valueOf(edge.getLabel().getText()));
                    if (!vertex.remainingFlowEdges.contains(e)) {
                        edge.getSource().remainingFlowEdges.add(e);
                        edge.getTarget().remainingFlowEdges.add(e.getInverted());
                    }
                }
            }

            do {
                source.bfsFrom = null;
                target.bfsFrom = null;
                Queue<Vertex> queue = new ArrayDeque<>();
                queue.add(source);
                source.bfsUsed = true;
                while (!queue.isEmpty()) {
                    Vertex vertex = queue.remove();
                    for (Edge edge : vertex.remainingFlowEdges) {
                        final Vertex target = edge.getTarget();
                        if (!target.bfsUsed && edge.capacity > edge.flow) {
                            target.bfsUsed = true;
                            target.bfsFrom = edge;
                            queue.add(target);
                        }
                    }
                }
                for (Vertex vertex : vertices) {
                    vertex.bfsUsed = false;
                }
                if (target.bfsFrom != null) {
                    Deque<Edge> path = new ArrayDeque<>();
                    path.add(target.bfsFrom);
                    while (path.getFirst().getSource().bfsFrom != null) {
                        path.addFirst(path.getFirst().getSource().bfsFrom);
                    }

                    double cmin = Collections.min(path, (a, b) -> (int) (a.capacity - b.capacity)).capacity;
                    for (Edge p : path) {
                        p.flow += cmin;
                        p.getInverted().flow -= cmin;
                    }
                    ans += cmin;
                }
            } while (target.bfsFrom != null);

            for (Vertex vertex : vertices) {
                vertex.remainingFlowEdges.clear();
            }
            maxFlow.setValue(ans);
        }
    }
}
