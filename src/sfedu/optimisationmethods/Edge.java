package sfedu.optimisationmethods;

import javafx.scene.control.TextField;
import javafx.scene.shape.Line;

public class Edge extends Line {

    private Vertex source, target;
    private TextField label;
    private Edge invertedEdge;
    protected double flow, capacity;

    public Edge(Vertex source) {
        this.source = source;
        target = null;
        flow = 0;
        capacity = 0;
        label = new TextField("0");
        label.setPrefColumnCount(2);
        startXProperty().bind(source.layoutXProperty());
        startYProperty().bind(source.layoutYProperty());
        setEndX(source.layoutXProperty().getValue());
        setEndY(source.layoutYProperty().getValue());
    }

    public Edge(Vertex source, Vertex target, Edge invertedEdge, int capacity) {
        this.source = source;
        this.target = target;
        if (invertedEdge == null) {
            invertedEdge = new Edge(target, source, this, capacity);
        }
        this.flow = 0;
        this.capacity = capacity;
        this.invertedEdge = invertedEdge;
    }

    public void setTargetX(double x) {
        setEndX(x);
    }

    public void setTargetY(double y) {
        setEndY(y);
    }

    public boolean setTarget(Vertex target) {
        this.target = target;
        endXProperty().bind(target.layoutXProperty());
        endYProperty().bind(target.layoutYProperty());
        label.layoutXProperty().bind(startXProperty().add(endXProperty()).divide(2));
        label.layoutYProperty().bind(startYProperty().add(endYProperty()).divide(2));
        invertedEdge = new Edge(target);
        invertedEdge.target = source;
        invertedEdge.label = label;
        invertedEdge.invertedEdge = this;
        if (!source.edges.contains(this)) {
            source.edges.add(this);
            target.edges.add(getInverted());
            return true;
        }
        return false;
    }

    public Edge getInverted() {
        return invertedEdge;
    }

    public TextField getLabel() {
        return label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        if (source != null ? !source.equals(edge.source) : edge.source != null) return false;
        return target != null ? target.equals(edge.target) : edge.target == null;

    }

    public Vertex getSource() {
        return source;
    }

    public Vertex getTarget() {
        return target;
    }
}
