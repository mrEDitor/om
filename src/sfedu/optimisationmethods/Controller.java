package sfedu.optimisationmethods;

import javafx.beans.binding.StringBinding;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private ToggleGroup toolGroup;

    @FXML
    private ToggleButton addEdgeTool;

    @FXML
    private ToggleButton addVertexTool;

    @FXML
    private Pane graphPane;

    @FXML
    public Label maxFlowValue;

    private FlowGraph graphModel;
    private Shape dragging;

    public Controller() {
        graphModel = new FlowGraph();
        dragging = null;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        graphPane.setOnMousePressed(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                onMousePressed(event.getX(), event.getY());
            }
        });
        graphPane.setOnMouseDragged(event -> onMouseDragged(event.getX(), event.getY()));
        graphPane.setOnMouseReleased(event -> onMouseReleased(event.getX(), event.getY()));
        graphModel.setView(graphPane);

        StringBinding maxFlowValueBinding = new StringBinding() {
            {
                bind(graphModel.maxFlowProperty());
            }

            @Override
            protected String computeValue() {
                DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                symbols.setInfinity("∞");
                DecimalFormat decimalFormat = new DecimalFormat("#", symbols);
                return decimalFormat.format(graphModel.maxFlowProperty().get());
            }
        };
        maxFlowValue.textProperty().bind(maxFlowValueBinding);

        addVertexTool.setSelected(true);
    }

    private void showContextMenu(Vertex vertex) {
        final MenuItem setSourceItem = new MenuItem("Сделать истоком");
        setSourceItem.setOnAction(event -> graphModel.setSource(vertex));
        final MenuItem setTargetItem = new MenuItem("Сделать стоком");
        setTargetItem.setOnAction(event -> graphModel.setTarget(vertex));
        final MenuItem deleteItem = new MenuItem("Удалить");
        deleteItem.setOnAction(event -> graphModel.deleteVertex(vertex));
        ContextMenu menu = new ContextMenu(setSourceItem, setTargetItem, deleteItem);
        final Point2D menuAnchor = graphPane.localToScreen(vertex.getLayoutX(), vertex.getLayoutY());
        menu.show(vertex, menuAnchor.getX(), menuAnchor.getY());
    }

    private void onMousePressed(double x, double y) {
        if (addVertexTool.isSelected()) {
            Vertex vertex = new Vertex();
            dragging = vertex;
            vertex.setOnMousePressed(event -> {
                final Vertex lVertex = (Vertex) event.getSource();
                if (event.getButton() == MouseButton.PRIMARY) {
                    if (addEdgeTool.isSelected()) {
                        dragging = new Edge(lVertex);
                        graphPane.getChildren().add(dragging);
                        dragging.toBack();
                    } else {
                        dragging = lVertex;
                    }
                } else {
                    showContextMenu(lVertex);
                }
            });
            vertex.setOnMouseReleased(event -> {
                Node picked = event.getPickResult().getIntersectedNode();
                if (dragging != null && picked instanceof Vertex && addEdgeTool.isSelected()) {
                    Edge edge = (Edge) dragging;
                    Vertex target = (Vertex) picked;
                    if (edge.setTarget(target)) {
                        TextField label = edge.getLabel();
                        graphPane.getChildren().add(label);
                        label.toFront();
                        label.setOnAction(e -> graphModel.refreshMaxFlow());
                        dragging = null;
                    }
                }
            });
            vertex.setLayoutX(x);
            vertex.setLayoutY(y);
            graphModel.addVertex(vertex);
        }
    }

    private void onMouseDragged(double x, double y) {
        if (dragging != null) {
            if (addEdgeTool.isSelected()) {
                ((Edge) dragging).setTargetX(x);
                ((Edge) dragging).setTargetY(y);
            } else {
                dragging.setLayoutX(x);
                dragging.setLayoutY(y);
            }
        }
    }

    private void onMouseReleased(double x, double y) {
        if (dragging != null) {
            if (addEdgeTool.isSelected()) {
                graphPane.getChildren().remove(dragging);
            } else {
                dragging.setLayoutX(x);
                dragging.setLayoutY(y);
            }
            dragging = null;
        }
    }
}
